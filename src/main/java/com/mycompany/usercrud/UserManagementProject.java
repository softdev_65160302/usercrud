/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.usercrud;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class UserManagementProject {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "Pass@1234", 'M', 'A');
        User user1 = new User("User 1", "User 1", "Pass@1234", 'M', 'U');
        User user2 = new User("User 2", "User 2", "Pass@1234", 'F', 'U');
        User user3 = new User("User 3", "User 3", "Pass@1234", 'F', 'U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        System.out.println(user3);
        User[] userArr = new User[4];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        userArr[3] = user3;
        System.out.println("print for Arr");
        //print for userArr
        for(int i =0;i<userArr.length;i++){
            System.out.println(userArr[i]);
        }
        
        System.out.println("Array List");
        //ArrayList
        ArrayList<User> userList = new ArrayList();
        userList.add(admin);
        System.out.println(userList.size()-1+" List Size "+userList.size());
        userList.add(user1);
        System.out.println(userList.size()-1+" List Size "+userList.size());
        userList.add(user2);
        System.out.println(userList.size()-1+" List Size "+userList.size());
        userList.add(user3);
        System.out.println(userList.size()-1+" List Size "+userList.size());
        for(int i = 0;i <userList.size();i++){
            System.out.println(userList.get(i));
        }
        User user4 = new User("User 4", "User 4", "Pass@1234", 'F', 'U');
        userList.set(0, user4);
        userList.remove(userList.size()-1);
        System.out.println(userList.get(0));
        for(User u: userList){
            System.out.println(u);
        }
        
    }
}
